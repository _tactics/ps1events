import datetime
import json

import requests
import dateutil.parser
import pytz


class Event(object):
    def __init__(self, title, start, end, author, authorEmail):
        self.title       = title
        self.start       = start
        self.end         = end
        self.author      = author
        self.authorEmail = authorEmail

    def __repr__(self):
        return 'Event(' + repr(self.title)       + ', ' + \
                          repr(self.start)       + ', ' + \
                          repr(self.end)         + ', ' + \
                          repr(self.author)      + ', ' + \
                          repr(self.authorEmail) + ')'

    def __str__(self):
        return str(self.title)


def all():
    caluri = "http://www.google.com/calendar/feeds/hhlp4gcgvdmifq5lcbk7e27om4@group.calendar.google.com/public/full"
    uri = caluri + "?alt=json&orderby=starttime&singleevents=true&sortorder=ascending&futureevents=true"
    r = requests.get(uri)
    jdata = json.loads(r.content)

    chitz = pytz.timezone('America/Chicago')

    for entry in jdata['feed']['entry']:
        yield Event(entry['title']['$t'], 
                    dateutil.parser.parse(entry['gd$when'][0]['startTime'])
                        .astimezone(chitz),
                    dateutil.parser.parse(entry['gd$when'][0]['endTime'])
                        .astimezone(chitz),
                    entry['author'][0]['name'],
                    entry['author'][0]['email'])


def today():
    chitz = pytz.timezone('America/Chicago')
    now = datetime.datetime.now(chitz)
    for ev in all():
        if ev.start.astimezone(chitz).date() == now.date():
            yield ev
        else:
            continue


def soon():
    chitz = pytz.timezone('America/Chicago')
    now = datetime.datetime.now(chitz)
    for ev in today():
        start = ev.start.astimezone(chitz)
        secsUntilEvent = (start - now).seconds
        minsUntilEvent = secsUntilEvent / 60
        if minsUntilEvent < 30:
            yield ev
        else:
            continue


def main():
    print 'All events:'
    for event in all():
        print '  ', event

    print "Today's events:"
    for event in today():
        print '   ', event

    print "Upcoming events:"
    for event in soon():
        print '   ', event


if __name__ == '__main__':
    main()

